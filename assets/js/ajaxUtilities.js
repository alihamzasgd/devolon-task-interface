/**
 * Author: Ali Hamza
 * Following file contains ajax calls, action listeners, data formatter and some utility javascript functions
 */
var api_base_url = "http://localhost:81/devolon-task/api";
var api_secret_key = 'DEVOLONCODEx@123';


// Function to delete a company
function deleteCompany(companyID) {
    // DELETE request to API for the deletion of specified resource
    $("#companies-loading-img").show();
    $.ajax({
        type: "DELETE",
        url: api_base_url + "/company/" + companyID,
        ContentType: "application/json",
        headers: {'x-api-key': api_secret_key},
        statusCode: {
            200: function (response) {
                $('#companies_table').DataTable().destroy();
                swal("Good job!", "Company deleted successfully.", "success");
                getCompanies();
                $("#companies-loading-img").hide();
            },
            404: function () {
                swal("Oops!", "No matching record found.", "error");
                $("#companies-loading-img").hide();
            }
        }
    });
}

// Function to delete a station
function deleteStation(stationID) {
    // DELETE request to API for the deletion of specified resource
    $("#stations-loading-img").show();
    $.ajax({
        type: "DELETE",
        url: api_base_url + "/station/" + stationID,
        ContentType: "application/json",
        headers: {'x-api-key': api_secret_key},
        statusCode: {
            200: function (response) {
                $('#stations_table').DataTable().destroy();
                swal("Good job!", "Station deleted successfully.", "success");
                getStations();
                $("#stations-loading-img").hide();
            },
            404: function () {
                swal("Oops!", "No matching record found.", "error");
                $("#stations-loading-img").hide();
            }
        }
    });
}


// Function to redirection to company edit page
function editCompany(companyID)
{
    window.location.href = "edit_companies.html?company_id="+companyID;
}

// Function to redirection to station edit page
function editStation(stationID)
{
    window.location.href = "edit_stations.html?station_id="+stationID;
}

// Function to All companies details for the table and drop-down
function getCompanies(companyID) {
    var companies_table_body = "";
    var companies_dropdown_options = "<option selected value=''>-- No Parent --</option>";
    $("#companies-loading-img").show();
    $.ajax({
        type: "GET",
        url: api_base_url + "/company",
        ContentType: "application/json",
        headers: {'x-api-key': api_secret_key},
        statusCode: {
            200: function (response) {
                var companies = response.data;
                companies.forEach(function (company) {
                    companies_table_body += "<tr>";
                    companies_table_body += "<td align='right'>" + company.id + "</td>";
                    companies_table_body += "<td >" + company.name + "</td>";
                    companies_table_body += "<td >" + company.parent_company_name + "</td>";
                    companies_table_body += "<td > <button class='btn btn-sm btn-info' onclick='editCompany(" + company.id + ")'>Edit</button>";
                    companies_table_body += "<button style='margin-left: 10px;' class='btn btn-sm btn-danger' onclick='deleteCompany(" + company.id + ")'>Delete</button></td>";
                    companies_table_body += "</tr>";

                    companies_dropdown_options += "<option value='" + company.id + "'>" + company.name + "</option>"
                });
                $("#companies_table_body").html(companies_table_body);
                $('#companies_table').DataTable();

                $("#parent_company_id").html(companies_dropdown_options);
                $("#companies-loading-img").hide();
            },
            404: function () {
                $("#companies-loading-img").hide();
            }
        }

    });
}

// Function to All stations and their details
function getStations() {
    var stations_table_body = "";
    $("#stations-loading-img").show();
    $.ajax({
        type: "GET",
        url: api_base_url + "/station",
        ContentType: "application/json",
        headers: {'x-api-key': api_secret_key},
        statusCode: {
            200: function (response) {
                var stations = response.data;
                stations.forEach(function (station) {
                    stations_table_body += "<tr>";
                    stations_table_body += "<td align='right'>" + station.id + "</td>";
                    stations_table_body += "<td >" + station.name + "</td>";
                    stations_table_body += "<td >" + station.parent_company_name + "</td>";
                    stations_table_body += "<td > <button class='btn btn-sm btn-info' onclick='editStation(" + station.id + ")'>Edit</button>";
                    stations_table_body += "<button style='margin-left: 10px;' class='btn btn-sm btn-danger' onclick='deleteStation(" + station.id + ")'>Delete</button></td>";
                    stations_table_body += "</tr>";
                });
                $("#stations_table_body").html(stations_table_body);
                $('#stations_table').DataTable();

                $("#stations-loading-img").hide();
            },
            404: function () {
                $("#stations-loading-img").hide();
            }
        }

    });
}


// Function to All companies details for the table and drop-down
function getCompanyDetails(companyID) {

    $.ajax({
        type: "GET",
        url: api_base_url + "/company/" + companyID,
        ContentType: "application/json",
        headers: {'x-api-key': api_secret_key},
        statusCode: {
            200: function (response) {
                var company = response.data;
                $("#name").val(company.name);
                if (company.parent_company_id != '' && company.parent_company_id != 0) {
                    $("#parent_company_id").val(company.parent_company_id);
                } else {
                    $("#parent_company_id").val('');
                }
            },
            404: function () {
            }
        }
    });
}

// Function to All companies details for the table and drop-down
function getStationDetails(stationID) {
    var promise = new Promise(function (resolve, reject) {
        $.ajax({
            type: "GET",
            url: api_base_url + "/station/" + stationID,
            ContentType: "application/json",
            headers: {'x-api-key': api_secret_key},
            statusCode: {
                200: function (response) {
                    var station = response.data;
                    $("#name").val(station.name);
                    $("#latitude").val(station.latitude);
                    $("#longitude").val(station.longitude);
                    if (station.company_id != '' && station.company_id != 0) {
                        $("#company_id").val(station.company_id);
                    } else {
                        $("#company_id").val('');
                    }
                    resolve();
                },
                404: function () {
                    resolve();
                }
            }
        });
    });
    return promise;
}



// Function to get companies for drop-down
function getCompaniesDropdown(targetDropDown) {
    var promise = new Promise(function (resolve, reject) {
        if (targetDropDown == "company_id") {
            var companies_dropdown_options = "<option selected value=''>-- select company --</option>";
        } else {
            var companies_dropdown_options = "<option selected value=''>-- No Parent --</option>";
        }
        $.ajax({
            type: "GET",
            url: api_base_url + "/company",
            ContentType: "application/json",
            headers: {'x-api-key': api_secret_key},
            statusCode: {
                200: function (response) {
                    var companies = response.data;
                    companies.forEach(function (company) {
                        companies_dropdown_options += "<option value='" + company.id + "'>" + company.name + "</option>"
                    });
                    $("#" + targetDropDown).html(companies_dropdown_options);
                    resolve();
                },
                400: function () {
                    $("#" + targetDropDown).html(companies_dropdown_options);
                    resolve();
                }
            }
        });
    });
    return promise;
}


// Function for form fields validation
function validateFormData() {
    var flag = true;
    var company_name = $("#name").val();
    var error_str = "";

    if (company_name == null || company_name == "") {
        error_str += "<li>Company name is required.</li>"
        flag = false;
    }

    if (flag == false) {
        $("#form_validation_error_ul").html(error_str);
        $("#form_validation_error_div").show();
    } else {
        $("#form_validation_error_ul").html("");
        $("#form_validation_error_div").hide();
    }
    return flag;
}

// Function for stations form fields validation
function validateFormDataStation(stationCreationFlag) {
    var flag = true;
    var station_radius;
    var station_name;

    if(stationCreationFlag == true) {
        station_name = $("#name").val();
    }else{
        station_radius = $("#radius").val();
    }
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();
    var company_id = $("#company_id").val();
    var error_str = "";

    if(stationCreationFlag == true) {
        if (station_name == null || station_name == "") {
            error_str += "<li>Station name is required.</li>"
            flag = false;
        }
    }else{
        if (station_radius == null || station_radius == "") {
            error_str += "<li>Search radius is required.</li>"
            flag = false;
        }
    }

    if (latitude == null || latitude == "") {
        error_str += "<li>Latitude is required.</li>"
        flag = false;
    }

    if (longitude == null || longitude == "") {
        error_str += "<li>Longitude is required.</li>"
        flag = false;
    }

    if (company_id == null || company_id == "") {
        error_str += "<li>Please select an owner company.</li>"
        flag = false;
    }

    if (flag == false) {
        $("#form_validation_error_ul").html(error_str);
        $("#form_validation_error_div").show();
    } else {
        $("#form_validation_error_ul").html("");
        $("#form_validation_error_div").hide();
    }

    return flag;
}


//Function to turn from data to a json object
function convertFormDataToJson(data) {
    var hash;
    var jsonData = {};
    var hashes = data.slice(data.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        jsonData[hash[0]] = hash[1];
    }
    return JSON.stringify(jsonData);
}


// Function to reset all input fields of a form
function resetForm(formID) {
    $(':input', '#' + formID)
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .prop('checked', false)
        .prop('selected', false);
}