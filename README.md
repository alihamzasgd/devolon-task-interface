# Task Details

Following repository contains source code of the web interface that consumes the RestFul API that can be cloned from the link below:

1. https://bitbucket.org/alihamzasgd/devolon-task/src/master/

## Development Technology Stack

* JavaScript/JQuery
* Bootstrap

## Please make sure to update API deployment BASE-URL in the JavaScript utilities file under following path:
application-sourcecode-folder/assets/js/ajaxUtilities.js

## Web Interface
Web application was built using Bootstrap, JQuery, JavaScript and some custom styling. Form validation is done through JavaScript. User is guided throughout the input process about error related to each input. JQuery Ajax request has been used to handle the flow of data without refreshing the browser page. JavaScript helper functions are defined to manipulated JSON data to the front-end.

### DataTables Library
DataTable library is used to paginate API results tables and provide ability of searching in the API results.

### JavaScript Promises
Promises feature of the JavaScript ECMAScript 6 standard has been followed to make sure the API calls are executed in wanted order keeping in mind asynchronous behavior of JavaScript.

### SweetAlert
SweetAlert library of JavaScript is used to show consent, success and error alert message in a presentable and interactive way.

### Google Maps JavaScript API
Google Maps JavaScript API has been used which enables end-user to select desired location from the map while station creation and Location based company stations search page.

# Online Resources Used:
1.	https://developers.google.com/maps/solutions/store-locator/clothing-store-locator#findnearsql
2.	https://en.wikipedia.org/wiki/Haversine_formula
3.	https://developers.google.com/web/fundamentals/primers/promises
4.	https://datatables.net/
5.	https://sweetalert.js.org/guides/


### Author
Ali Hamza
